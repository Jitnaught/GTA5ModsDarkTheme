[Original userstyle by Nick Brealey.](https://userstyles.org/styles/120550/gta5-mods-dark-theme)

Mostly just fixes, but also some improvements for elements which weren't colored.
I also fixed the coloring for admins/moderators (I was a moderator on the site).